# SchoolTimetable
![SchoolTimetable](https://codeberg.org/hackedtogethersoftware/SchoolTimetable/raw/branch/master/schooltimetablelogo.png)

An application to view the current day's timetable using timetable files from Sentral.

## Installation
Currently Linux only, go to the [Releases](https://codeberg.org/hackedtogethersoftware/SchoolTimetable/releases) and download the binary (it's just a single binary, no need for installation script or whatever). You may need to `chmod +x path/to/SchoolTimetable.linux` to give it executing permissions, then run it and you are good to go.

## Building
See [BUILDING.md](https://codeberg.org/hackedtogethersoftware/SchoolTimetable/src/branch/master/BUILDING.md)
