extends Control

@onready var init := $"/root/Init"

func _ready():
    init.userpath = OS.get_config_dir() + "/SchoolTimetable/"
    init.settingsfile = init.userpath + "settings.json"
    print(init.userpath)
    print(init.settingsfile)
    var settingsdir = DirAccess.open(OS.get_config_dir())
    if !settingsdir.dir_exists(init.userpath):
        settingsdir.make_dir(init.userpath)
        print("created data/config path at " + init.userpath)
    # var platform := OS.get_name()
    # if platform == "Windows":
    #     init.userpath = OS.get_system_dir()
    # elif platform == "OSX":
    #     pass
    # elif platform == "X11":
    #     pass
    DisplayServer.window_set_min_size(Vector2i(350,250))
    if FileAccess.file_exists(init.settingsfile):
        var settingsfile := FileAccess.open(init.settingsfile,FileAccess.READ)
        if settingsfile == null:
            print("could not load settings file")
        else:
            init.settings = JSON.parse_string(settingsfile.get_as_text())
    else:
        var settingsfile := FileAccess.open(init.settingsfile,FileAccess.WRITE)
        if settingsfile == null:
            print("could not create settings file")
        else:
            init.settings = init.defaultsettings
            settingsfile.store_string(JSON.stringify(init.settings))
    if FileAccess.file_exists(init.userpath + "timetable.json"):
        var file := FileAccess.open(init.userpath + "timetable.json",FileAccess.READ)
        if file == null:
            print("oh no timetable was not able to be opened for some reason, do you have the privileges to view the appdata folder?")
        var result = JSON.parse_string(file.get_as_text())
        if typeof(result) != TYPE_ARRAY:
            get_tree().call_deferred("change_scene_to_file", "res://noneloaded.tscn")
        else:
            init.timetable = result
            get_tree().call_deferred("change_scene_to_file", "res://mainui.tscn")
    else:
        get_tree().call_deferred("change_scene_to_file", "res://noneloaded.tscn")
