extends Control
const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
]
const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]
const periodcolor = Color8(0,85,212)
const breakcolor = Color8(255,255,255)
const margin12 = -65

var barpositions := []
var currperiod := -1
var tableexists := false
var periodstart := [-1,-1]
var periodend := [-1,-1]
var indicator = 0
var hovering := false
var mouseinwindow := true
var needsscrolling := false
var scrollbuffer := 0

@onready var init = $"/root/Init"

func _ready():
    init.currdate = [-1,-1,-1]

func _on_settingsbutton_pressed():
    if get_tree().change_scene_to_file("res://settings.tscn") != OK:
        print("something went wrong when loading the ui")

func _process(_delta):
    var datetime = Time.get_datetime_dict_from_system()
    # datetime["hour"] = 9
    # datetime["minute"] = 4
    # datetime["second"] = 00
    var today = date()
    # today = [2024,2,1] # manually overriding the date
    # datetime["year"] = today[0]
    # datetime["month"] = today[1]
    # datetime["day"] = today[2]
    # if datetime["minute"] >= 32:
    #     today[2] = 22
    if today != init.currdate:
        tableexists = false
        $timetablearea/scroll.hide()
        $timetablearea/none.hide()
        init.currdate = today
        init.todayindex = -1
        for i in range(len(init.timetable)):
            if "date" in init.timetable[i]:
                if comparray(init.timetable[i]["date"], today):
                    init.todayindex = i
                    break
        if init.todayindex == -1:
            $timetablearea/none.show()
            var lastdate = init.timetable[-1]["date"]
            var expired = (
                today[0] > lastdate[0]
                || (
                    today[0] == lastdate[0] && (
                        today[1] > lastdate[1]
                        || (
                            today[1] == lastdate[1]
                            && today[2] > lastdate[2]
                        )
                    )
                )
            )
            if expired:
                $timetablearea/none/expired.show()
            else:
                $timetablearea/none/expired.hide()
        else: # when there is an actual timetable for the day.
            tableexists = true
            init.todayperiods = eventstoperiods(init.timetable[init.todayindex]["events"])
            if len(init.todayperiods[0]) == 0:
                $timetablearea/none.show()
            else:
                $timetablearea/scroll.show()
                indicator = preload("res://tableelements/indicator.tscn").instantiate()
                for child in $timetablearea/scroll.get_children():
                    child.propagate_call("queue_free",[])
                $timetablearea/scroll.add_child(indicator)
                indicator.offset_top = 5
                indicator.offset_bottom = 5
                rendertimetable()
    $bg/Time.text = timetostr([datetime["hour"],datetime["minute"]])
    $bg/Weekday.text = weekdays[datetime["weekday"]-1]
    $bg/Weekday/Date.text = months[datetime["month"]-1] + " " + str(datetime["day"])
    if tableexists:
        currperiod = -1
        $starttime.show()
        $endtime.show()
        for i in init.todayperiods[1]:
            if datetime["hour"] < i[0] || (datetime["hour"] == i[0] && datetime["minute"] < i[1]):
                break
            else:
                currperiod += 1
        # currperiod = 5 # override the period displayed

        # Hide everything first, then show the one you want
        $bg/bc.hide()
        $bg/ac.hide()
        $bg/Period.hide()
        $bg/breaktext.hide()
        if currperiod == -1:
            $bg/bc.show()
            $progress.disabled = true
            $starttime.text = ""
            $endtime.text = ("-"
                + inttotimespan(
                    timetoint(init.todayperiods[1][0])
                    - timetoint([datetime["hour"],datetime["minute"],datetime["second"]])
                )
            )
        elif currperiod >= len(init.todayperiods[0]):
            $bg/ac.show()
            $progress.disabled = true
            $endtime.text = ""
            $starttime.text = ("+"
                + inttotimespan(
                    timetoint([datetime["hour"],datetime["minute"],datetime["second"]])
                    - timetoint(init.todayperiods[1][-1])
                )
            )
        else:
            $progress.disabled = false
            indicator.offset_top = barpositions[currperiod]
            indicator.offset_bottom = barpositions[currperiod+1]
            if typeof(init.todayperiods[0][currperiod]["period"]) == TYPE_INT:
                indicator.texture.gradient.colors[0] = breakcolor
                indicator.texture.gradient.colors[1] = breakcolor
                indicator.texture.gradient.colors[1].a = 0.0
                $bg/breaktext.show()
            else:
                indicator.texture.gradient.colors[0] = periodcolor
                indicator.texture.gradient.colors[1] = periodcolor
                indicator.texture.gradient.colors[1].a = 0.0
                $bg/Period/Class.text = init.todayperiods[0][currperiod]["class"]
                if (init.todayperiods[0][currperiod]["period"].begins_with("P") &&
                    init.todayperiods[0][currperiod]["period"].substr(1).is_valid_int()):
                    $bg/Period.text = "Period " + init.todayperiods[0][currperiod]["period"].substr(1)
                else:
                    $bg/Period.text = init.todayperiods[0][currperiod]["period"]
                $bg/Period/Class/Teacher.text = init.todayperiods[0][currperiod]["teacher"]
                $bg/Period/Location.text = init.todayperiods[0][currperiod]["location"]
                $bg/Period.show()
            periodstart = init.todayperiods[1][currperiod]
            periodend = init.todayperiods[1][currperiod+1]
            var currtimeint: int = timetoint([datetime["hour"],datetime["minute"],datetime["second"]])
            var dstart: int = currtimeint - timetoint(periodstart)
            var dend: int = timetoint(periodend) - currtimeint
            $progress.value = float(dstart)/(timetoint(periodend)-timetoint(periodstart))
            if hovering && mouseinwindow:
                $starttime.text = "+" + inttotimespan(dstart)
                $endtime.text = "-" + inttotimespan(dend)
            else:
                $starttime.text = timetostr(periodstart)
                $endtime.text = timetostr(periodend)
        needsscrolling = barpositions[-1]+5 > $timetablearea.size.y
    else:
        $starttime.hide()
        $endtime.hide()
        $bg/Period.hide()
        $bg/ac.hide()
        $bg/bc.hide()
        $progress.disabled = true
        needsscrolling = false
    if needsscrolling:
        $timetablearea/scroll.offset_top = clamp(
            $timetablearea/scroll.offset_top + scrollbuffer,
            -(barpositions[-1]+6 - $timetablearea.size.y),
            0
        )
        scrollbuffer = 0
    else:
        $timetablearea/scroll.offset_top = 0



func date() -> Array:
    var time = Time.get_date_dict_from_system()
    return [time["year"],time["month"],time["day"]]

func timetostr(time:Array):
    if "goodhour" in init.settings:
        if init.settings["goodhour"]:
            return zfill(time[0],2) + ":" + zfill(time[1],2)
        else:
            if time[0] == 0:
                return "12:" + zfill(time[1],2) + "AM"
            elif time[0] < 12:
                return str(time[0]) + ":" + zfill(time[1],2) + "AM"
            elif time[0] == 12:
                return "12:" + zfill(time[1],2) + "PM"
            else:
                return str(time[0]-12) + ":" + zfill(time[1],2) + "PM"

    else:
        return zfill(time[0],2) + ":" + zfill(time[1],2)

func timetoint(time:Array) -> int:
    var result = time[0]*3600+time[1]*60
    if len(time) >= 3:
        result += time[2]
    return result

func inttotimespan(time:int):
    var hour = time/3600
    var minute = (time%3600)/60
    var second = (time%60)
    var result = ""
    if hour:
        result += zfill(hour,2) + "h "
        result += zfill(minute,2) + "m "
    elif minute:
        result += zfill(minute,2) + "m "
    result += zfill(second,2) + "s"
    return result

func eventstoperiods(events:Array) -> Array: # converts the events from the json into periods and breaks
    var periods := [[],[]]
    var reqkeys := [
        "start",
        "end",
        "period",
        "class",
        "teacher"
    ]
    var prevend = [-1,-1,-1]
    for i in range(len(events)):
        if has_keys(events[i],reqkeys):
            if len(periods[0]) != 0:
                if !comparray(events[i]["start"],prevend):
                    periods[0].append(
                        {
                            "period":-1,
                        }
                    )
                    periods[1].append(prevend)
            periods[0].append(
                {
                    "period":events[i]["period"],
                    "class":events[i]["class"],
                    "teacher":events[i]["teacher"],
                    "location":events[i]["location"] if "location" in events[i] else ""
                }
            )
            periods[1].append(events[i]["start"])
            prevend = events[i]["end"]
    periods[1].append(prevend)
    return periods

func zfill(number: int, digits: int):
    var string = str(number)
    return "0".repeat(max(digits - len(string),0)) + string

func comparray(x:Array,y:Array):
    if len(x) == len(y):
        var thing = true
        for i in range(len(x)):
            if x[i] != y[i]:
                thing = false
                break
        return thing
    else:
        return false

func has_keys(dict:Dictionary,keys:Array):
    for i in keys:
        if !(i in dict):
            return false
    return true

func rendertimetable(): # instances all the things in the timetable display
    barpositions = [] # the positions of the bars, so the period indicator can be bounded correctly
    var ypos := 5 # the y position of the top of the period.
    for i in range(len(init.todayperiods[0])):
        var bar = preload("res://tableelements/bar.tscn").instantiate()
        $timetablearea/scroll.add_child(bar)
        bar.offset_top = ypos
        if "goodhour" in init.settings:
            if !init.settings["goodhour"]:
                bar.get_node("bar").offset_right = margin12
        barpositions.append(ypos)
        bar.get_node("time").text = timetostr(init.todayperiods[1][i])
        if typeof(init.todayperiods[0][i]["period"]) == TYPE_INT:
            var breakperiod = preload("res://tableelements/break.tscn").instantiate()
            $timetablearea/scroll.add_child(breakperiod)
            breakperiod.offset_top += ypos
            breakperiod.offset_bottom += ypos
            ypos += breakperiod.size.y
        else:
            var event = preload("res://tableelements/event.tscn").instantiate()
            $timetablearea/scroll.add_child(event)
            event.offset_top += ypos
            event.offset_bottom += ypos
            event.get_node("period").text = init.todayperiods[0][i]["period"]
            event.get_node("class").text = init.todayperiods[0][i]["class"]
            event.get_node("location").text = init.todayperiods[0][i]["location"]
            event.get_node("teacher").text = init.todayperiods[0][i]["teacher"]
            if init.todayperiods[0][i]["location"] == "":
                event.get_node("teacher").grow_vertical = GROW_DIRECTION_BOTH
            ypos += event.size.y
            if "goodhour" in init.settings:
                if !init.settings["goodhour"]:
                    event.offset_right = margin12 - 5
    # instance the final bar
    var finalbar = preload("res://tableelements/bar.tscn").instantiate()
    $timetablearea/scroll.add_child(finalbar)
    finalbar.offset_top = ypos
    finalbar.get_node("time").text = timetostr(init.todayperiods[1][-1])
    if "goodhour" in init.settings:
        if !init.settings["goodhour"]:
            finalbar.get_node("finalbar").offset_right = margin12
    barpositions.append(ypos)

func _on_hoverarea_mouse_entered():
    hovering = true

func _on_hoverarea_mouse_exited():
    hovering = false

func _on_ScrollableArea_gui_input(event):
    if event is InputEventMouseButton and needsscrolling:
        var buttonindex = event.get_button_index()
        if buttonindex == 4:
            scrollbuffer += init.settings["scrollsensitivity"]*event.factor
        elif buttonindex == 5:
            scrollbuffer -= init.settings["scrollsensitivity"]*event.factor

func _notification(what):
    if what == NOTIFICATION_WM_MOUSE_EXIT:
        mouseinwindow = false
    elif what == NOTIFICATION_WM_MOUSE_ENTER:
        mouseinwindow = true
