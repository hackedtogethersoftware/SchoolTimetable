# Building
These are build instructions for SchoolTimetable. For now I am only including instructions for Linux because I haven't figured out the other OSes yet.

First you must obtain the source for this by either going to the releases page or cloning this repo. From now on we will call the directory `schooltimetable`. Navigate to this directory in your shell.

## Dependencies
Install the Godot Engine (current version for this build is 4.2.1)

## Custom export template (optional)
First, if you want, build a smaller export template of the Godot engine, as this application doesn't require some of the advanced functionality like the 3D engine, so this will reduce the file size.

Obtain the source for Godot
```bash
cd ..
git clone https://github.com/godotengine/godot.git -b 4.2.1-stable --depth 1 godot4
cd godot4
```
Build using this command
```bash
scons target=template_release tools=no lto=full optimize=size disable_3d=yes module_text_server_adv_enabled=no module_text_server_fb_enabled=yes
```
Now the export template will appear in `godot/bin`. Do
```bash
cd ../schooltimetable
mkdir -p export_templates
cp ../godot4/bin/godot.linuxbsd.template_release.x86_64 export_templates/ # this is the export template
```
Note that the file name for the export template may vary, depending on architecture or whatever, so just check what it is and put it there.

## Actually building the thing
Open the project with Godot. You can do this by opening Godot and then clicking "Import" and navigating to the `schooltimetable/` directory in the file dialog and selecting it. To compile the project, go to `Project > Export` and add the Linux preset. Now click on the appeared `Linux/X11 (Runnable)`. If you decided to build the custom export template, then set the `Custom Template > Release` by clicking on the folder icon and navigating to `schooltimetable/export_templates/schooltimetable_minimal.build` (once again the file name may vary). Enable `Binary Format > Embed PCK`. Click `Export Project`, navigate to wherever you want the binary to be exported to, and uncheck `Export with Debug`. Now you can proceed and build, the resulting binary should work.

## Other platforms
Hopefully this section is temporary and I figure out how to build this properly for Windows and MacOS. For now if you are on those platforms you can just install Godot, open the project, and export normally. This should work fine, but unfortunately the program will be very obese.
