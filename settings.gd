extends Control

@onready var init = $"/root/Init"

func _ready():
    $bg/optiongrid/TimeFormat.add_item("12 hour")
    $bg/optiongrid/TimeFormat.add_item("24 hour")
    $bg/optiongrid/TimeFormat.selected = int(init.settings["goodhour"])
    $bg/optiongrid/scrollsens.value = init.settings["scrollsensitivity"]

func savesettings():
    init.settings["goodhour"] = bool($bg/optiongrid/TimeFormat.selected)
    init.settings["scrollsensitivity"] = $bg/optiongrid/scrollsens.value
    var settingsfile = FileAccess.open(init.settingsfile, FileAccess.WRITE)
    if settingsfile == null:
        print("could not open settings file")
    else:
        settingsfile.store_string(JSON.stringify(init.settings))

func _notification(what):
    if (what == NOTIFICATION_WM_CLOSE_REQUEST):
        savesettings()
        get_tree().quit()

func _on_backbutton_pressed():
    savesettings()
    if get_tree().change_scene_to_file("res://mainui.tscn") != OK:
        print("something went wrong when loading the ui")

func _on_loadbutton_pressed():
    $loaddialog.popup()

func _on_loaddialog_file_selected(path):
    var ical := FileAccess.open(path,FileAccess.READ)
    if ical == null:
        failmessage()
    else:
        var caljson = FileAccess.open(init.userpath + "timetable.json",FileAccess.WRITE)
        if caljson == null:
            failmessage()
        else:
            var result = load("res://icalparser.gd").parseical(ical)
            if typeof(result) == TYPE_ARRAY:
                init.timetable = result
                caljson.store_string(JSON.stringify(init.timetable))
                successmessage()
            else:
                failmessage()
        caljson.close()
    ical.close()

func failmessage():
    $bg/loadbutton/fail.show()
    $bg/loadbutton/success.hide()
    $bg/loadbutton/successtimer.stop()
    $bg/loadbutton/failtimer.stop()
    $bg/loadbutton/failtimer.start()

func successmessage():
    $bg/loadbutton/fail.hide()
    $bg/loadbutton/success.show()
    $bg/loadbutton/successtimer.stop()
    $bg/loadbutton/failtimer.stop()
    $bg/loadbutton/successtimer.start()

func _on_successtimer_timeout():
    $bg/loadbutton/success.hide()

func _on_failtimer_timeout():
    $bg/loadbutton/fail.hide()

func _on_about_pressed():
    $About.show()

func _on_TextureButton_pressed():
    $About.hide()

func _on_closeinfo_pressed():
    $Info.hide()

func _on_info_pressed():
    $Info.show()
