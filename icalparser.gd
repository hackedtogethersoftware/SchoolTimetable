extends Node
static func parseical(file:FileAccess):
    file.seek(0)
    var events := []
    var timezone := (Time.get_time_zone_from_system()["bias"] as int)
    var currfull := [-1,-1,-1,-1,-1]                                                               # all of the setup for the arrays
    var currdate := [-1,-1,-1]
    var currtime := [-1,-1]
    var endfull := [-1,-1,-1,-1,-1]
    while not file.eof_reached():                                                                  # iterate on the lines
        var line := file.get_line().strip_edges()
        var lastexists := (len(events) != 0)
        if lastexists:
            lastexists = ("events" in events[-1])
        if lastexists:
            lastexists = (len(events[-1]["events"]) != 0)
        if line:
            if line.begins_with("DTSTART:"):                                       # BEGIN TIME
                currfull = ztotime(line.substr(8).strip_edges(),timezone)                         # set the current full date/time
                if currfull.slice(0,3) != currdate:                                                # new day
                    events.append({"date": currfull.slice(0,3),"events":[]})                       # create the new day
                    currdate = currfull.slice(0,3)                                                 # set the current date to the new one
                currtime = currfull.slice(3,5)                                                    # set the time
                events[-1]["events"].append({"start":currtime})                                    # create the event in the current day
            elif line.begins_with("DTEND:") && lastexists:                         # END TIME
                endfull = ztotime(line.substr(6).strip_edges(),timezone)                          # set the end full date/time
                events[-1]["events"][-1]["end"] = endfull.slice(3,5)                              # set the end time
            elif line.begins_with("SUMMARY:") && lastexists:
                events[-1]["events"][-1]["class"] = line.split(":")[1]                             # get the class abbreviation
            elif line.begins_with("LOCATION:") && lastexists:
                var location := line.split(":")
                if len(location) >= 3:
                    events[-1]["events"][-1]["location"] = location[2].strip_edges()               # get the location
                else:
                    events[-1]["events"][-1]["location"] = ""
            elif line.begins_with("DESCRIPTION:") && lastexists:                                   # DESCRIPTION
                var lbindex := line.find("\\n")                                                    # get the position of the "\n"
                if lbindex != -1:
                    events[-1]["events"][-1]["teacher"] = line.substr(20,lbindex-20).strip_edges() # teacher
                    var period := line.substr(lbindex+2).split(":")[1].strip_edges()               # get period text
                    if period.is_valid_int():
                        events[-1]["events"][-1]["period"] = "P" + period                          # regular period
                    else:
                        events[-1]["events"][-1]["period"] = period                                # non-numerical period
    return events

static func ztotime(string:String, bias:int) -> Array:
    var year := int(string.substr(0,4))
    var month := int(string.substr(4,2))
    var day := int(string.substr(6,2))
    var hour := int(string.substr(9,2))
    var minute := int(string.substr(11,2))
    minute += bias
    hour += fdiv(minute,60)
    minute = imod(minute,60)
    day += fdiv(hour,24)
    hour = imod(hour,24)
    var months := [31,28,31,30,31,30,31,31,30,31,30,31]
    if year%4 == 0:
        months[1] = 29
    var tempmonth := month
    month += fdiv(day-1,months[month-1])
    day = imod(day-1,months[tempmonth-1])+1
    year += fdiv(month-1,12)
    month = imod(month-1,12)+1
    return [year,month,day,hour,minute]

static func imod(x:int,y:int) -> int:
    var thing := x % y
    if thing >= 0:
        return thing
    else:
        return thing+y

static func fdiv(x:int,y:int) -> int:
    var thing := x / y
    if x >= 0:
        return thing
    else:
        return thing-1
