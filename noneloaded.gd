extends Control

func _on_loadbutton_pressed():
    $loaddialog.popup()

func _on_loaddialog_file_selected(path):
    print(path)
    var ical := FileAccess.open(path,FileAccess.READ)
    if ical == null:
        $bg/loadbutton/fail.show()
        $bg/loadbutton/failtimer.start()
    else:
        var caljson := FileAccess.open($"/root/Init".userpath + "timetable.json",FileAccess.WRITE)
        if caljson == null:
            $bg/loadbutton/fail.show()
            $bg/loadbutton/failtimer.start()
        else:
            $"/root/Init".timetable = load("res://icalparser.gd").parseical(ical)
            caljson.store_string(JSON.stringify($"/root/Init".timetable))
            ical.close()
            caljson.close()
            if get_tree().change_scene_to_file("res://mainui.tscn") != OK:
                print("something went wrong when loading the ui")

func _on_failtimer_timeout():
    $bg/loadbutton/fail.hide()

func _on_info_pressed():
    $Info.show()

func _on_closeinfo_pressed():
    $Info.hide()
