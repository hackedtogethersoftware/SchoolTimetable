@tool
extends Panel
@export var value := 0.0
@export var disabled := false

func _process(_delta):
    self.material.set_shader_parameter("progress",value)
    self.material.set_shader_parameter("disabled",disabled)
